"""
Майдуров Артём Игоревич, КИ22-17/1Б
Практическая работа №4, Вариант 19
"""

from random import randint


def qsort(lst: list) -> list:
    """
    Функция быстрой сортировки

    Examples:
        >>> qsort([2, 3, 1])
        [1, 2, 3]

    :param lst: сортируемый список
    :return: отсортированный список
    :raises: TypeError
    """
    if len(lst) <= 1:
        return lst
    if not all([isinstance(i, (int, float, str)) for i in lst]):
        raise TypeError("Невозможно отсортировать нечисловые "
                        "или нестроковые значения")
    current_type = type(lst[0])  # Проверка на тип данных
    if current_type in (int, float):
        current_type = (int, float)
    if not all([isinstance(i, current_type) for i in lst]):
        raise TypeError("Невозможно сравнить числовые и "
                        "строковые типы данных")

    elem = lst.pop(len(lst) // 2)
    more_than_elem = []
    less_than_elem = []
    for x in lst:
        if x > elem:
            more_than_elem.append(x)
        else:
            less_than_elem.append(x)
    return [*qsort(less_than_elem), elem, *qsort(more_than_elem)]


def check_arg(line: str) -> int:
    """
    Tries to convert line to int, if it is possible, else raises ValueError

    Examples:
        >>> check_arg("123")
            123

    :param line: some string which we want tp convert
    :return: converted string
    :raises: ValueError
    """
    try:
        return int(line)
    except ValueError:
        print("Требуется ввести число")


def generate_list() -> list:
    """
    Generates list of int
    :return: list of integer
    """
    print("Введите длину, минимальное и максимальное значение"
          " для генерации на разных строках")
    length = check_arg(input())
    min_number = check_arg(input())
    max_number = check_arg(input())
    if length and max_number and min_number:
        return [randint(int(min_number), int(max_number))
                for _ in range(int(length))]
    else:
        print("Ошибка ввода, список не будет сгенерирован")


def main():
    lst = None
    while True:
        print("Доступны следующие команды:\n"
              "1) Сгенерировать список\n"
              "2) Отсортировать\n"
              "3) Выход")
        command = input()
        if command.lower() == "выход" or command == "3":
            return
        elif command.lower() == "сгенерировать список" or command == "1":
            lst = generate_list()
            print(lst)
        elif command.lower() == "отсортировать" or command == "2":
            if lst:
                try:
                    lst = qsort(lst)
                    print(lst)
                except TypeError as ex:
                    print(ex)
            else:
                print("Список сначала нужно сгенерировать")
        else:
            print("Вы допустили ошибку во вводе")


def example():
    """
    An example function
    """
    print(qsort([10, -1, 8, 4, 7]))
    try:
        print(qsort([10, -1, 8, 4, "a"]))
    except TypeError as ex:
        print(ex)
    print(qsort([]))
    print(qsort(["b", "a"]))
    print(qsort([1.1, 1]))
    try:
        print(qsort([[1.1], 1]))
    except TypeError as ex:
        print(ex)


if __name__ == "__main__":
    main()
